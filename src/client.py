"""
Description:
Author:
Email:
"""
import socket

def client_program():
    """ This function contains client cose of socket programming"""
    # Since both server and client running in the same computer
    # same IP is obtained by accessing the hostname
    host = socket.gethostname() # sever IP
    port = 5000 # Server port

    # Initialize the socket with default values
    client_socket = socket.socket()

    # Connect to the server
    client_socket.connect((host, port))

    # Input message
    message = input("->")
    if len(message) > 0:
        message = message.lower().strip()
        while message != 'bye':
            # Send encoded message
            client_socket.send(message.encode())

            # Receive data packets sent from the server
            data = client_socket.recv(1024)
            print(f"Received From server: {data.decode()}")

            # Again take the input
            message = input("->")

    # Close connection
    client_socket.close()

    return True

if __name__ == "__main__":
    client_program()