"""
Description:
Author: Barnabas Augustino Matonya
Email: barnabasaugustino@gmail.com
"""

import socket

def server_progarm():
    """ This function contains the server codes"""
    # Get hostname and the port
    host = socket.gethostname() # Get host name
    port = 5000 # The port number should be above 1024

    # Initialize the socket instance
    server_socket = socket.socket()
    # Bind socket IP address to a port  umber
    server_socket.bind((host, port))

    # Configure how many clients the server cal listen simultaneously
    server_socket.listen(2)

    # Accept connection
    conn, address = server_socket.accept()
    print("Connection from", str(address))

    while True:
        # Receive data streams, The data packets should be less or eaqual to 1024 bytes
        encoded_data = conn.recv(1024)
        # Decode data
        data = encoded_data.decode()
        print(f"Recived: {str(data)} form user: {str(address)}")

        # Send a response to the client
        data = input('->')
        response = data.encode()
        conn.send(response)

        # If "bye" is received then break the loop
        if not data:
            break
    # Close connection
    conn.close()
    return True


if __name__ == "__main__":
    server_progarm()

